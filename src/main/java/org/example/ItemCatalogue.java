package org.example;

import java.math.BigDecimal;
import java.util.*;

public enum ItemCatalogue {

    APPLE(0.35),
    BANANA(0.20),
    MELON(0.50),
    LIME(0.15),
    OTHER(0.0);

    public double price;

    private static final Map<String, ItemCatalogue> lookup = new HashMap<>();

    ItemCatalogue(double price) {
        this.price = price;
    }

    public static ItemCatalogue fromString(String item) {
        return lookup.getOrDefault(item.toLowerCase().trim(), ItemCatalogue.OTHER);
    }

    static {
        EnumSet.allOf(ItemCatalogue.class).forEach(type -> {
            lookup.put(type.toString().toLowerCase().trim(), type);
        });
    }
}
