package org.example;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This class is used to calculate the total price of the shopping basket
 *
 * @author Pradeep
 *
 */
public class App 
{

    /**
     * Method to calculate the price of shopping purchased items
     * @param items - list of items in shopping basket
     */
    public static BigDecimal calculatePriceOfShoppingBasket(List<String> items) {
        BigDecimal totalCost = BigDecimal.ZERO;
        Map<String, Long> itemsGroup = Optional.ofNullable(items).orElseGet(ArrayList::new).stream().
                filter(Objects::nonNull).
                map(String::toLowerCase).
                collect(
                Collectors.groupingBy(Function.identity(), Collectors.counting())
        );

        for (Map.Entry<String, Long> item : itemsGroup.entrySet()) {
            ItemCatalogue itemCatalogue = ItemCatalogue.fromString(item.getKey());
            switch (itemCatalogue) {
                case APPLE:
                case BANANA:
                    totalCost = totalCost.add(BigDecimal.valueOf(item.getValue()).multiply(BigDecimal.valueOf(itemCatalogue.price)));
                    break;
                case MELON:
                    // 2 melons for 1
                    totalCost = totalCost.add(BigDecimal.valueOf(Math.ceil(Double.valueOf(item.getValue()) / 2)).multiply(BigDecimal.valueOf(itemCatalogue.price)));
                    break;
                case LIME:
                    // 3 lime for 2
                    totalCost = totalCost.add(BigDecimal.valueOf((((item.getValue() / 3) * 2) + (item.getValue() % 3))).multiply(BigDecimal.valueOf(itemCatalogue.price)));
                    break;
            }
        }
        return totalCost;
    }
}
