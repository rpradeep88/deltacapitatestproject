package org.example;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

/**
 * Unit test for simple App.
 */
public class AppTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }

    public void testCalcluateItemsInShoppingBasket() {
        BigDecimal totalCost = App.calculatePriceOfShoppingBasket(Arrays.asList("Apple", "Banana", "Melon", "Lime"));
        assertEquals(1.20, totalCost.doubleValue());
    }

    public void testCalculateItemsInEmptyBasket() {
        BigDecimal totalCost = App.calculatePriceOfShoppingBasket(Collections.emptyList());
        assertEquals(0.0, totalCost.doubleValue());
    }

    public void testCalcluateItemsWithFiveMelons() {
        BigDecimal totalCost = App.calculatePriceOfShoppingBasket(Arrays.asList("MELON", "Melon", "Melon", "Melon", "Melon"));
        assertEquals(1.5, totalCost.doubleValue());
    }

    public void testCalCulateItemsWithFourLime() {
        BigDecimal totalCost = App.calculatePriceOfShoppingBasket(Arrays.asList("Lime", "Lime", "Lime", "Lime"));
        assertEquals(0.45, totalCost.doubleValue());
    }

    public void testInvalidItemType() {
        BigDecimal totalCost = App.calculatePriceOfShoppingBasket(Arrays.asList("Apple", "TEST"));
        assertEquals(0.35, totalCost.doubleValue());
    }

    public void testNullArray() {
        BigDecimal totalCost = App.calculatePriceOfShoppingBasket(null);
        assertEquals(0.0, totalCost.doubleValue());
    }
}
